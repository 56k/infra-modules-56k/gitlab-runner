# gitlab-runner

Terraform code to deploy a Gitlab runner

## Built the gitlab runner AMI

We use a custom AMI for this module. To build it follow the instructions here: https://gitlab.com/56k/packer/-/tree/master/gitlab-runner
