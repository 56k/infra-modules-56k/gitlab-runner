/*
This module deploys a Gitlab Runner using a custom AMI we have built with Packer
*/

/*
CREATE AN INSTANCE PROFILE AND ROLE FOR THE GITLAB RUNNER
*/
resource "aws_iam_instance_profile" "gitlab_runner_profile" {
  name = "gitlab_runner_profile"
  role = aws_iam_role.gitlab_runner_role.name
}

resource "aws_iam_role" "gitlab_runner_role" {
  name = "gitlab_runner_role"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "gitlab_runner_policy" {
  name = "gitlab_runner_policy"
  role = aws_iam_role.gitlab_runner_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetRepositoryPolicy",
        "ecr:DescribeRepositories",
        "ecr:ListImages",
        "ecr:DescribeImages",
        "ecr:BatchGetImage",
        "ecr:InitiateLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:CompleteLayerUpload",
        "ecr:PutImage",
        "ecs:*",
        "ssm:Describe*",
        "ssm:Get*",
        "ssm:List*",
        "cloudformation:*",
        "cloudwatch:*",
        "cognito-identity:ListIdentityPools",
        "cognito-sync:GetCognitoEvents",
        "cognito-sync:SetCognitoEvents",
        "dynamodb:*",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:DescribeVpcs",
        "events:*",
        "iam:*",
        "lambda:*",
        "logs:*",
        "s3:*",
        "sns:ListSubscriptions",
        "sns:ListSubscriptionsByTopic",
        "sns:ListTopics",
        "sns:Publish",
        "sns:Subscribe",
        "sns:Unsubscribe",
        "sqs:ListQueues",
        "sqs:SendMessage",
        "states:*",
        "apigateway:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

/*
LAUNCH THE GITLAB RUNNER
*/

resource "aws_instance" "gitlab_runner" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id
  user_data                   = data.template_file.user_data.rendered
  associate_public_ip_address = true
  key_name                    = var.ssh_key_name
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_sg.id]
  iam_instance_profile        = aws_iam_instance_profile.gitlab_runner_profile.name

  root_block_device {
    volume_type = "gp2"
    volume_size = var.root_disk_size
    encrypted   = var.root_device_encrypted
  }

  tags = {
    Name = "pg-alpha-${var.gitlab_env}-gitlab-runner"
    Environment = var.environment
    Application = var.app_name
  }
}

/*
THE USER DATA SCRIPT THAT WILL WILL RUN ON THE GITLAB RUNNER DURING BOOT
*/
data "template_file" "user_data" {
  template = "${file("${path.module}/user-data/user-data.sh")}"

  vars = {
    env   = var.gitlab_env
    token = var.gitlab_token
  }
}

/*
CREATE A DNS A RECORD FOR THE SERVER
Create an A Record in Route 53 pointing to the IP of this server so you can connect to it using a nice domain name
like foo.your-company.com.
*/

resource "aws_route53_record" "gitlab-runner" {
  zone_id = var.dns_zone_id
  name    = var.domain_name
  type    = "A"
  ttl     = "300"
  records = [aws_instance.gitlab_runner.public_ip]
}
