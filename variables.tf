/*
Input variables used to configure the 'gitlab-runner' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC in which to launch the Gitlab Runner"
}

variable "subnet_id" {
  type        = string
  description = "The subnet in which to launch the Gitlab Runner"
}

variable "allowed_ingress_cidrs" {
  type        = list
  description = "The IP address ranges allowed to access this instance on SSH"
}

variable "instance_type" {
  type        = string
  description = "The type of instance to run for the Gitlab runner"
}

variable "ami_id" {
  type        = string
  description = "The AMI to run on the Gitlab runner"
}

variable "ssh_key_name" {
  type        = string
  description = "The name of a Key Pair that can be used to SSH to this instance."
}

variable "dns_zone_id" {
  type        = string
  description = "The DNS zone file ID in which to create the Gitlab Runner hostname"
}

variable "domain_name" {
  type        = string
  description = "The domain name to use for the Gitlab Runner"
}

variable "root_disk_size" {
  type        = number
  description = "The size of the root disk of the Gitlab runner"
}

variable "root_device_encrypted" {
  type        = bool
  description = "If the root device should be encrypted or not"
}

variable "gitlab_env" {
  type        = string
  description = "The environment in which this gitlab runner will be deployed. It will be added as a tag to the runner"
}

variable "gitlab_token" {
  type        = string
  description = "The token to be used by this runner to register itself in Gitlab"
}

variable "force_destroy" {
  type        = bool
  default     = false
  description = "When a terraform destroy is run, should the backup s3 bucket be destroyed even if it contains files. Should only be set to true for testing/development"
}
