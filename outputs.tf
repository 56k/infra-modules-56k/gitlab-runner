/*
Outputs from the 'gitlab-runner' module
*/

output "public_ip" {
  value = aws_instance.gitlab_runner.public_ip
}

output "private_ip" {
  value = aws_instance.gitlab_runner.private_ip
}
