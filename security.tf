/*
Security groups for the 'gitlab-runner' module
*/
resource "aws_security_group" "gitlab_runner_sg" {
  name        = "gitlab_runner_sg"
  description = "Ports needed by the Gitlab runners"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.allowed_ingress_cidrs
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}
